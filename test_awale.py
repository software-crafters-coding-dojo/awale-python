import pytest

from awale import Plateau, GRAINES_INITIALES, NOMBRE_TROUS_PAR_JOUEUR


def test_nouvelle_partie():
    plateau = Plateau()
    for trou in range(NOMBRE_TROUS_PAR_JOUEUR):
        assert plateau.trous_joueur_1[trou].graines == GRAINES_INITIALES
        assert plateau.trous_joueur_2[trou].graines == GRAINES_INITIALES

def test_premier_move_trou_1():
    plateau = Plateau()
    plateau.joue(1)

    assert plateau.trous_joueur_1[0].graines == 0
    for trou in range(1,5):
        assert plateau.trous_joueur_1[trou].graines == 5

def test_premier_move_trou_2():
    plateau = Plateau()
    plateau.joue(2)

    assert plateau.trous_joueur_1[1].graines == 0
    for trou in range(2,NOMBRE_TROUS_PAR_JOUEUR):
        assert plateau.trous_joueur_1[trou].graines == 5

def test_premier_move_trou_3():
    plateau = Plateau()
    plateau.joue(3)

    assert plateau.trous_joueur_1[2].graines == 0
    for trou in range(3,NOMBRE_TROUS_PAR_JOUEUR):
        assert plateau.trous_joueur_1[trou].graines == 5
    assert plateau.trous_joueur_2[0].graines == 5

def test_second_move():
    plateau = Plateau()

    plateau.joue(1)
    plateau.joue(1)

    for trou in range(1,NOMBRE_TROUS_PAR_JOUEUR-1):
        assert plateau.trous_joueur_1[trou].graines == 5
        assert plateau.trous_joueur_2[trou].graines == 5

def test_debordement():
    plateau = Plateau()

    plateau.joue(5)
    plateau.joue(6)

    assert plateau.trous_joueur_1[0].graines == 5
    assert plateau.trous_joueur_1[1].graines == 5
    assert plateau.trous_joueur_1[2].graines == 5
    assert plateau.trous_joueur_1[3].graines == 5
    assert plateau.trous_joueur_2[5].graines == 0
