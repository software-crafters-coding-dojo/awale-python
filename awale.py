from enum import Enum

GRAINES_INITIALES = 4
NOMBRE_TROUS_PAR_JOUEUR = 6  

class Trou:
    def __init__(self, graines):
        self.graines = graines


class Joueur(Enum):
    JOUEUR_1 = 0
    JOUEUR_2 = NOMBRE_TROUS_PAR_JOUEUR



class Plateau:
    def __init__(self):
        self.trous = [Trou(GRAINES_INITIALES) for _ in range(NOMBRE_TROUS_PAR_JOUEUR * 2)]
        self.index_joueur_courant = 0
        self.joueur_courant = Joueur.JOUEUR_1

    def joue(self, trou):

        graines_a_distribuer = GRAINES_INITIALES
        index_trou = trou + self.joueur_courant.value

        self.trous[index_trou-1].graines = 0

        while (graines_a_distribuer>0):
            self.trous[index_trou].graines = 5
            graines_a_distribuer = graines_a_distribuer-1
            index_trou = index_trou+1
            if index_trou == 12:
                index_trou = 0

        self.joueur_courant = Joueur.JOUEUR_2

    @property
    def trous_joueur_1(self):
        return self.trous[:NOMBRE_TROUS_PAR_JOUEUR]

    @property
    def trous_joueur_2(self):
        return self.trous[NOMBRE_TROUS_PAR_JOUEUR:]